db.rooms.insertMany(
[
{
    name: "double",
    accomodates: 3,
    price: 2000,
    description: "A room fir for a small family going on a vacation",
    rooms_available: 5,
    isAvailable: false 
},
{
    accomodates: 4,
    price: 4000,
    description: "A room with a queen sized bed perfect for a simple getaway",
    rooms_available: 15,
    isAvailable: false 
}
]
)
